﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exam.ViewModel;
using Exam.Model;
using Exam.Services;

namespace Exam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _viewModel;
        private ValidationService _validationService = new ValidationService();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel = SerializationService.DeSerializeMainWindowVM();
            _viewModel.LoadProjects();
            this.DataContext = _viewModel;
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            SerializationService.SerializeMainWindowVM(_viewModel);
        }

        public Project GetSelectedProject()
        {
            var selectedproject = this.lboxProjects.SelectedItem as Project;
            return selectedproject;
        }
        public Project GetProjectDataFromUI(Project project)
        {
            project.Name = txtProjectName.Text;
            project.Manager = txtManagerName.Text;
            project.Description = txtProjectDescription.Text;
            project.AddedTime = DateTime.Now;
            return project;
        }
        public void SetProjectDataForUI(Project project)
        {
            this.txtProjectName.Text = project.Name;
            this.txtManagerName.Text = project.Manager;
            this.txtProjectDescription.Text = project.Description;
        }
        public Activity GetSelectedActivity()
        {
            var selectedactivity = this.lboxActivity.SelectedItem as Activity;
            return selectedactivity;
        }
        public Activity GetActivityDataFromUI(Activity activity)
        {
            activity.Name = this.txtActivityName.Text;
            activity.Employee = this.txtEmploee.Text;
            activity.Description = this.txtActivityDescription.Text;
            activity.Start = this.dateFromDate.SelectedDate.Value;
            activity.End = this.dateToDate.SelectedDate.Value;
            return activity;
        }
        public void SetActivityDataForUI(Activity activity)
        {
            this.txtActivityName.Text = activity.Name;
            this.txtEmploee.Text = activity.Employee;
            this.txtActivityDescription.Text = activity.Description;
            this.dateFromDate.SelectedDate = activity.Start;
            this.dateToDate.SelectedDate = activity.End;
        }
        private void btnAddActivity_Click(object sender, RoutedEventArgs e)
        {
            if (_validationService.ValidateGrid(gridActivityInformation))
            {
                if (GetSelectedActivity()!=null)
                {
                    this.GetActivityDataFromUI(GetSelectedActivity());
                    this.lboxActivity.SelectedIndex = -1;
                }
                else 
                {
                    _viewModel.AddActivity(this.GetSelectedProject(), this.GetActivityDataFromUI(new Activity()));
                }
            }
        }

        private void btnRemoveActivity_Click(object sender, RoutedEventArgs e)
        {
            if (lboxActivity.SelectedIndex>-1)
            {
                _viewModel.RemoveActivity(lboxProjects.SelectedItem as Project,lboxActivity.SelectedItem as Activity);
            }
        }

        private void btnAddOrUpdateProject_Click(object sender, RoutedEventArgs e)
        {
            if (_validationService.ValidateGrid(gridProjectInformation))
            {
                if (GetSelectedProject() != null)
                {
                    this.GetProjectDataFromUI(GetSelectedProject());
                    this.lboxProjects.SelectedIndex = -1;
                }
                else if (!_viewModel.AddProject(this.GetProjectDataFromUI(new Project())))
                {
                    MessageBox.Show("Unable to add new project. Project already exists.");
                }
            }
        }

        private void btnRemoveProject_Click(object sender, RoutedEventArgs e)
        {
            if (lboxProjects.SelectedIndex > -1)
            {
                _viewModel.RemoveProject(lboxProjects.SelectedItem as Project);
            }
        }

        private void btnSearchProject_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtProjectName.Text)
                && string.IsNullOrEmpty(this.txtManagerName.Text)
                && string.IsNullOrEmpty(this.txtProjectDescription.Text))
            {
                _viewModel.SearchProject("");
            }
            if (!string.IsNullOrEmpty(this.txtProjectName.Text))
            {
                _viewModel.SearchProject(this.txtProjectName.Text);
            }
            if(!string.IsNullOrEmpty(this.txtManagerName.Text))
            {
                _viewModel.SearchProject(this.txtManagerName.Text);
            }
            if(!string.IsNullOrEmpty(this.txtProjectDescription.Text))
            {
                _viewModel.SearchProject(this.txtProjectDescription.Text);
            }
        }

        private void lboxProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lboxProjects.SelectedIndex>-1)
            {
                this.SetProjectDataForUI(lboxProjects.SelectedItem as Project);
                this.btnAddOrUpdateProject.Content = "Update project";
                this.gridActivityInformation.Visibility = Visibility.Visible;
                this.pnlActivityButtons.Visibility = Visibility.Visible;
            }
            else
            {
                this.btnAddOrUpdateProject.Content = "Add new project";
                this.gridActivityInformation.Visibility = Visibility.Collapsed;
                this.pnlActivityButtons.Visibility = Visibility.Collapsed;
            }
        }

        private void lboxActivity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lboxActivity.SelectedIndex>-1)
            {
                this.SetActivityDataForUI(this.lboxActivity.SelectedItem as Activity);
            }
        }
    }
}
