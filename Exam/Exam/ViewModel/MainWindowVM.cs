﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exam.Model;

namespace Exam.ViewModel
{
    [Serializable()]
    public class MainWindowVM:NotificationBase
    {
        private ObservableCollection<Project> _projects;
        private ObservableCollection<Project> _projectlist;

        public ObservableCollection<Project> Projectlist
        {
            get
            {
                return _projectlist;
            }
        }
        public ObservableCollection<Project> Projects
        {
            get
            {
                return _projects;
            }

            set
            {
                _projects = value;
                NotifyPropertyChanged("Projects");
            }
        }

        public MainWindowVM()
        {
            _projects = new ObservableCollection<Project>();
            _projectlist = new ObservableCollection<Project>();
        }
        public bool AddProject(Project project)
        {
            var existingproject = _projectlist.Where(x=>x.Name==project.Name);
            if (existingproject.Any())
            {
                return false;
            }else
            {
                _projectlist.Add(project);
                return true;
            }
        }
        public void RemoveProject(Project project)
        {
            _projects.Remove(project);
        }
        public void AddActivity(Project project, Activity activity)
        {
            project.AddActivity(activity);
        }
        public void RemoveActivity(Project project, Activity activity)
        {
            project.RemoveActivity(activity);
        }
        public void SearchProject(string name)
        {
            var list = _projectlist.Where(x => x.Name.ToLower().Contains(name.ToLower())).ToList();
            Projects = new ObservableCollection<Project>(list);
        }
        public void LoadProjects()
        {

            Projects = _projectlist;
        }
    }
}
