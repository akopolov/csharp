﻿using Exam.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Services
{
    public class SerializationService
    {
        private static string _fileName = "Data.bin";
        public static MainWindowVM DeSerializeMainWindowVM()
        {
            try
            {
                using (var stream = new FileStream(_fileName, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    if (stream.Length.Equals(0))
                    {
                        return new MainWindowVM();
                    }
                    else
                    {
                        var binaryFormatter = new BinaryFormatter();
                        var vm = binaryFormatter.Deserialize(stream) as MainWindowVM;
                        return vm;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error: " + ex.Message);
                return null;
            }
        }

        public static void SerializeMainWindowVM(object o)
        {
            using (Stream stream = new FileStream(_fileName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, o);
            }
        }
    }
}
