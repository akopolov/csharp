﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Exam.Services
{
    public class ValidationService
    {
        public bool ValidateGrid(Grid grid)
        {
            var children = grid.Children;
            bool validation = true;
            foreach(var chield in children)
            {
                if (chield.GetType()==typeof(TextBox))
                {
                    var currentchield = chield as TextBox;
                    if (currentchield.Text.Length<1)
                    {
                        MessageBox.Show(currentchield.Name);
                        validation = false;
                    }
                }
                if (chield.GetType()==typeof(DatePicker))
                {
                    var currentchield = chield as DatePicker;
                    if (!currentchield.SelectedDate.HasValue)
                    {
                        MessageBox.Show("Please select Date");
                        validation = false;
                    }
                }
            }
            return validation;
        }
    }
}
