﻿using System;
using Exam.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Exam.Model
{
    [Serializable()]
    public class Project:NotificationBase
    {
        private string _name;
        private string _description;
        private string _manager;
        private DateTime _addedtime;
        private TimeSpan _totaltime;
        private ObservableCollection<Activity> _activitylist; 

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }
        public DateTime AddedTime
        {
            get
            {
                return _addedtime;
            }
            set
            {
                _addedtime = value;
                NotifyPropertyChanged("AddedTime");
            }
        }
        public string Manager
        {
            get
            {
                return _manager;
            }

            set
            {
                _manager = value;
                NotifyPropertyChanged("Manager");
            }
        }
        public ObservableCollection<Activity> Activitylist
        {
            get
            {
                return _activitylist;
            }
        }
        public TimeSpan Totaltime
        {
            get
            {
                _totaltime = new TimeSpan(0, 0, 0);
                if (_activitylist!=null) {
                    foreach (Activity activ in _activitylist)
                    {
                        _totaltime += activ.Timespeend;        
                    }
                }
                return _totaltime;
            }
        }
        public Project()
        {
            this._activitylist = new ObservableCollection<Activity>();
        }
        public void AddActivity(Activity activity)
        {
            this._activitylist.Add(activity);
            base.NotifyPropertyChanged("Totaltime");
        }
        public void RemoveActivity(Activity activity)
        {
            this._activitylist.Remove(activity);
            base.NotifyPropertyChanged("Totaltime");
        }
    }
}
