﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam.Model
{
    [Serializable()]
    public class Activity
    {
        private string _name;
        private string _description;
        private string _employee;
        private DateTime _start;
        private DateTime _end;
        private TimeSpan _timespeend;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }
        public string Employee
        {
            get
            {
                return _employee;
            }

            set
            {
                _employee = value;
            }
        }
        public DateTime Start
        {
            get
            {
                return _start;
            }

            set
            {
                _start = value;
            }
        }
        public DateTime End
        {
            get
            {
                return _end;
            }

            set
            {
                _end = value;
            }
        }
        public TimeSpan Timespeend
        {
            get
            {
                _timespeend = _end.Subtract(_start);
                return _timespeend;
            }
        }
    }
}
