﻿using System.Collections.Generic;
using BL.Domain;
using BL.Service;
using System.Linq;

namespace Bank.ViewModels
{
    public class UserListViewModel
    {
        private AdminService _adminService;

        public UserListViewModel()
        {
            _adminService = new AdminService();
        }

        public IEnumerable<User_type> UserTypes { get; set; }
        public IEnumerable<User> Users { get; set; }
        public Pager Pages { get; set; }

        public void FillVM(int? page)
        {
            var user_list = _adminService.GetUsersList();
            var usertype_list = _adminService.GetUserTypeList();
            Pages = new Pager(user_list.Count(), page);
            Users = user_list.Skip((Pages.CurrentPage - 1) * Pages.PageSize).Take(Pages.PageSize);
            UserTypes = usertype_list.Skip((Pages.CurrentPage - 1) * Pages.PageSize).Take(Pages.PageSize);
        }
    }
}