﻿using BL.Domain;
using BL.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.ViewModels
{
    public class LoggerViewModel
    {
        private AdminService _adminService;
        public LoggerViewModel()
        {
            _adminService = new AdminService();
        }
        public IEnumerable<BL.Domain.Action> ActionList { get; set; }
        public Pager Pages { get; set; }

        public void FillVM(int? page)
        {
            var list = _adminService.GetLoggerList();            
            Pages = new Pager(list.Count(), page);
            ActionList = list.Skip((Pages.CurrentPage - 1) * Pages.PageSize).Take(Pages.PageSize);
        }
    }
}