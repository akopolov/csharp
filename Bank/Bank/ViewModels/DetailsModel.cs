﻿using System.Collections.Generic;
using BL.Domain;
using BL.Service;

namespace Bank.ViewModels
{
    public class DetailsModel
    {
        private AdminService _adminService;

        public DetailsModel()
        {
            _adminService = new AdminService();
        }

        public User User { get; set; }
        public IEnumerable<User_type> UserType { get; set; }
        public IEnumerable<Account> Account { get; set; }

        public void FillVM(int id)
        {
            User = _adminService.GetUser(id);
            UserType = _adminService.GetUserType(id);
            Account = _adminService.GetUserAccounts(id);
        }
    }
}