﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BL.Service;
using System.Linq;
using BL.Domain;

namespace Bank.ViewModels
{
    public class PaymentViewModel
    {
        private AccountService _accountService = new AccountService();
        private TransactionService _transactionService = new TransactionService();

        [AmountValidation]
        [Required]
        [RegularExpression(@"\d+(\,\d{1,2})?", ErrorMessage = "Invalid price")]
        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Display(Name = "Your account")]
        [SenderAccNrValidation]
        public string SenderAccNr { get; set; }

        [Display(Name = "Receiver account")]
        [ReceiverAccNrValidation]
        public string ReceiverAccNr { get; set; }

        [Display(Name = "Sender")]
        public string SenderName { get; set; }

        [Display(Name = "Receiver")]
        public string ReceiverName { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }

        public IEnumerable<SelectListItem> SenderAccList { get; set; }

        [Display(Name = "Receiver account")]
        public IEnumerable<SelectListItem> AllAccList { get; set; }


        public void FillVM()
        {
            // get user accounts list
            SenderAccList = _accountService.GetUserAccLists()
                .Select(i => new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i
                });
            // get all accounts list
            AllAccList = _accountService.GetAllAccLists()
                .Select(i => new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i
                });

            // Fill senderName and receiverName if we know the accounts
            if (SenderAccNr != null && ReceiverAccNr != null)
            {
                SenderName = _accountService.GetUserByAccountNr(SenderAccNr).User_firstname
                    + " " + _accountService.GetUserByAccountNr(SenderAccNr).User_lastname;
                ReceiverName = _accountService.GetUserByAccountNr(ReceiverAccNr).User_firstname
                    + " " + _accountService.GetUserByAccountNr(ReceiverAccNr).User_lastname;
            }

            if (Comment == null)
            {
                Comment = "";
            }
        }


        // Perform transaction between accounts
        public void MakeTransaction()
        {
            _transactionService.MakeTransaction(SenderAccNr, ReceiverAccNr, Amount, Comment);
        }
    }


    // Helper class for amount validation.
    public class AmountValidation : ValidationAttribute
    {
        private AccountService _accountService = new AccountService();

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PaymentViewModel paymentVM = (PaymentViewModel)validationContext.ObjectInstance;
            // Check if amount is positive
            if (paymentVM.Amount <= 0)
            {
                return new ValidationResult("Amount must be positive.");
            }

            Account acc = _accountService.GetAccountByAccNr(paymentVM.SenderAccNr);
            // Check if account is selected; for balance check
            if (acc == null)
            {
                return new ValidationResult("Account must be selected.");
            }

            // Check if account has sufficient funds
            if (acc.Account_balance - paymentVM.Amount < 0)
            {
                return new ValidationResult("Insufficient funds on account: " + paymentVM.SenderAccNr);
            }

            return ValidationResult.Success;
        }
    }

    // Helper class for sender account selection validation.
    public class SenderAccNrValidation : ValidationAttribute
    {
        private AccountService _accountService = new AccountService();

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PaymentViewModel vm = (PaymentViewModel)validationContext.ObjectInstance;

            Account acc = _accountService.GetAccountByAccNr(vm.SenderAccNr);
            // Check if account is selected
            if (acc == null)
            {
                return new ValidationResult("You must select an account.");
            }

            return ValidationResult.Success;
        }
    }

    // Helper class for receiver account selection validation.
    public class ReceiverAccNrValidation : ValidationAttribute
    {
        private AccountService _accountService = new AccountService();

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PaymentViewModel vm = (PaymentViewModel)validationContext.ObjectInstance;

            Account acc = _accountService.GetAccountByAccNr(vm.ReceiverAccNr);
            // Check if account is selected
            if (acc == null)
            {
                return new ValidationResult("You must select an account.");
            }

            return ValidationResult.Success;
        }
    }
}