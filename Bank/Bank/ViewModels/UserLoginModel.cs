﻿using System.ComponentModel.DataAnnotations;

namespace Bank.ViewModels
{
    public class UserLoginModel
    {
        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Login")]
        [StringLength(20)]
        public string UserLogin { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password")]
        [StringLength(20, MinimumLength = 9, ErrorMessage = "Password is 9-20 characters long")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        public string ResponseMessage { get; set; }
    }
}