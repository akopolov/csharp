﻿using BL.BussinessObjects;
using BL.Service;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bank.ViewModels
{
    public class TransactionHistoryResultViewModel
    {
        private TransactionService _transactionService = new TransactionService();
        private AccountService _accountService = new AccountService();


        [Display(Name = "Your account")]
        public string UserAccNr { get; set; }

        public IEnumerable<TransactionHistory> HistoryList { get; set; }

        public double CurrentValue { get; set; }
        public double StartValue { get; set; }

        public void FillVM(string accNr)
        {
            UserAccNr = accNr;
            CurrentValue = _accountService.GetAccountByAccNr(accNr).Account_balance;
            HistoryList = _transactionService.GetTransactionHistory(accNr);
            StartValue = _transactionService.GetTransactionStartValue(accNr);
        }
    }
}