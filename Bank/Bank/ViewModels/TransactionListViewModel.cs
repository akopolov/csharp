﻿using System.Collections.Generic;
using BL.Domain;
using BL.Service;
using System.Linq;

namespace Bank.ViewModels
{
    public class TransactionListViewModel
    {
        private AdminService _adminService;
        public TransactionListViewModel()
        {
            _adminService = new AdminService();
        }
        public IEnumerable<Transaction> TransactionList { get; set; }
        public Pager Pages { get; set; }
        public void FillVM(int? page)
        {
            var list = _adminService.GetTransactionsList();
            Pages = new Pager(list.Count(), page);
            TransactionList = list.Skip((Pages.CurrentPage - 1) * Pages.PageSize).Take(Pages.PageSize);
        }
    }
}