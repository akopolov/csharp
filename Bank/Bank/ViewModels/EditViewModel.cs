﻿using System.Collections.Generic;
using BL.Domain;
using BL.Service;

namespace Bank.ViewModels
{
    public class EditViewModel
    {
        private AdminService _adminService;

        public EditViewModel()
        {
            _adminService = new AdminService();
        }

        public User UserInfo { get; set; }
        public User_type UserTypeConnection { get; set; }
        public IEnumerable<Status> Statuses { get; set; }
        public IEnumerable<Type_u> Types { get; set; }

        public void FillVM(int id)
        {
            UserInfo = _adminService.GetUser(id);
            UserTypeConnection = _adminService.GetUserTypeOne(id);
            Statuses = _adminService.GetStatusList();
            Types = _adminService.GetUserTypes();
        }
    }
}