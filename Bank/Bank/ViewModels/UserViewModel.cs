﻿using BL.Domain;
using BL.Service;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Bank.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(20, MinimumLength = 9, ErrorMessage = "Password must be 9-20 characters long.")]
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [CheckConfirm_password]
        public string ConfirmPassword { get; set; }
        public string ErrorMessage { get; set; }

        public void FillVM(User u, string errorMessage)
        {
            User = u;
            ErrorMessage = errorMessage;
        }
    }
    //Validation 
    //Compare User_password and Confrim_password
    public class CheckConfirm_password : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var user = (UserViewModel)validationContext.ObjectInstance;
            if (user.ConfirmPassword == user.User.User_password)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("Password does not match the confirm password");
        }
    }
}