﻿using BL.Domain;
using BL.Service;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Bank.ViewModels
{
    public class TransactionHistoryViewModel
    {
        private AccountService _accountService = new AccountService();

        [Display(Name = "Select your account")]
        [AccNrValidation]
        public string AccNr { get; set; }

        public IEnumerable<SelectListItem> AccList { get; set; }

        public void FillVM()
        {
            // TODO siia peaks vist isegi nö "kustutatud" accounte välja näitama?
            // get user accounts list
            AccList = _accountService.GetUserAccLists()
                .Select(i => new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i
                });
        }
    }

    // Helper class for account selection validation.
    public class AccNrValidation : ValidationAttribute
    {
        private AccountService _accountService = new AccountService();

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            TransactionHistoryViewModel vm = (TransactionHistoryViewModel)validationContext.ObjectInstance;

            Account acc = _accountService.GetAccountByAccNr(vm.AccNr);
            // Check if account is selected
            if (acc == null)
            {
                return new ValidationResult("You must select an account.");
            }

            return ValidationResult.Success;
        }
    }
}