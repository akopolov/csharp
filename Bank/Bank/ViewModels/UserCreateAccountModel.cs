﻿using BL.Service;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bank.ViewModels
{
    public class UserCreateAccountModel
    {
        private AccountService _accountService = new AccountService();

        [Required(ErrorMessage = "Balance is required")]
        [Display(Name = "Account balance")]
        [RegularExpression(@"\d+(\,\d{1,2})?", ErrorMessage = "Invalid price")]
        [BalanceValidation]
        public double AccountBalance { get; set; }

        // Save new account to DB
        internal void SaveAccount()
        {
            _accountService.SaveAccount(AccountBalance);
        }

    }

    public class BalanceValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            UserCreateAccountModel acc = (UserCreateAccountModel)validationContext.ObjectInstance;
            if (acc.AccountBalance <= 0)
            {
                return new ValidationResult("Balance must be positive.");
            }

            return ValidationResult.Success;
        }
    }
}