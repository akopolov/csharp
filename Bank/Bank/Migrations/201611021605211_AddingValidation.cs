namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "User_login", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.User", "User_password", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.User", "User_firstname", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.User", "User_lastname", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "User_lastname", c => c.String(maxLength: 20));
            AlterColumn("dbo.User", "User_firstname", c => c.String(maxLength: 20));
            AlterColumn("dbo.User", "User_password", c => c.String(maxLength: 20));
            AlterColumn("dbo.User", "User_login", c => c.String(maxLength: 20));
        }
    }
}
