namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Status_id = c.Int(nullable: false, identity: true),
                        Status_name = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.Status_id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        User_id = c.Int(nullable: false, identity: true),
                        User_login = c.String(maxLength: 20),
                        User_password = c.String(maxLength: 20),
                        User_firstname = c.String(maxLength: 20),
                        User_lastname = c.String(maxLength: 20),
                        Online = c.Boolean(nullable: false),
                        Status_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.User_id)
                .ForeignKey("dbo.Status", t => t.Status_id, cascadeDelete: true)
                .Index(t => t.Status_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User", "Status_id", "dbo.Status");
            DropIndex("dbo.User", new[] { "Status_id" });
            DropTable("dbo.User");
            DropTable("dbo.Status");
        }
    }
}
