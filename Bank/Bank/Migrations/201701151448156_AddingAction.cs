namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingAction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Action",
                c => new
                    {
                        Action_ID = c.Int(nullable: false, identity: true),
                        Action_date = c.DateTime(nullable: false),
                        Action_type = c.String(nullable: false),
                        Info = c.String(nullable: false),
                        User_ID = c.Int(),
                        Account_ID = c.Int(),
                        Transaction_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Action_ID)
                .ForeignKey("dbo.Account", t => t.Account_ID)
                .ForeignKey("dbo.Transaction", t => t.Transaction_ID)
                .ForeignKey("dbo.User", t => t.User_ID)
                .Index(t => t.User_ID)
                .Index(t => t.Account_ID)
                .Index(t => t.Transaction_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Action", "User_ID", "dbo.User");
            DropForeignKey("dbo.Action", "Transaction_ID", "dbo.Transaction");
            DropForeignKey("dbo.Action", "Account_ID", "dbo.Account");
            DropIndex("dbo.Action", new[] { "Transaction_ID" });
            DropIndex("dbo.Action", new[] { "Account_ID" });
            DropIndex("dbo.Action", new[] { "User_ID" });
            DropTable("dbo.Action");
        }
    }
}
