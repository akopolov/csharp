namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingAccounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Account_id = c.Int(nullable: false, identity: true),
                        User_id = c.Int(nullable: false),
                        Account_number = c.String(),
                        Account_balance = c.String(),
                    })
                .PrimaryKey(t => t.Account_id)
                .ForeignKey("dbo.User", t => t.User_id, cascadeDelete: true)
                .Index(t => t.User_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Account", "User_id", "dbo.User");
            DropIndex("dbo.Account", new[] { "User_id" });
            DropTable("dbo.Account");
        }
    }
}
