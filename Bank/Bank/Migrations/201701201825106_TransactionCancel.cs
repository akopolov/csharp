namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransactionCancel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transaction", "Cancel", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transaction", "Cancel");
        }
    }
}
