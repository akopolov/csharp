namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingUserTypeConnection : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User_type",
                c => new
                    {
                        User_type_ID = c.Int(nullable: false, identity: true),
                        User_ID = c.Int(nullable: false),
                        Type_ID = c.Int(nullable: false),
                        From_date = c.DateTime(nullable: false),
                        To_date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.User_type_ID)
                .ForeignKey("dbo.Type_u", t => t.Type_ID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID)
                .Index(t => t.Type_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User_type", "User_ID", "dbo.User");
            DropForeignKey("dbo.User_type", "Type_ID", "dbo.Type_u");
            DropIndex("dbo.User_type", new[] { "Type_ID" });
            DropIndex("dbo.User_type", new[] { "User_ID" });
            DropTable("dbo.User_type");
        }
    }
}
