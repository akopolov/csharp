namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class account_isActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "IsActive");
        }
    }
}
