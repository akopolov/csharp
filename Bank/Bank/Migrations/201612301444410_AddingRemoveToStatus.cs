namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingRemoveToStatus : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Status(Status_Name)VALUES('Removed')");
        }
        
        public override void Down()
        {
        }
    }
}
