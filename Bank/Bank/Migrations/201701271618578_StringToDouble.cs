namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StringToDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Account", "Account_balance", c => c.Double(nullable: false));
            AlterColumn("dbo.Transaction", "Value", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transaction", "Value", c => c.String());
            AlterColumn("dbo.Account", "Account_balance", c => c.String());
        }
    }
}
