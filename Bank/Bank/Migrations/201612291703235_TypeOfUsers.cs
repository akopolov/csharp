namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TypeOfUsers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Type_u",
                c => new
                    {
                        Type_ID = c.Int(nullable: false, identity: true),
                        Type_name = c.String(),
                    })
                .PrimaryKey(t => t.Type_ID);

            Sql("INSERT INTO Type_u(Type_name)VALUES('Admin')");
            Sql("INSERT INTO Type_u(Type_name)VALUES('User')");
        }
        
        public override void Down()
        {
            DropTable("dbo.Type_u");
        }
    }
}
