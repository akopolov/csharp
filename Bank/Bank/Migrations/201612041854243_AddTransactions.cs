namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transaction",
                c => new
                    {
                        Transactions_id = c.Int(nullable: false, identity: true),
                        Transaction_date = c.DateTime(nullable: false),
                        Sender_id = c.Int(),
                        Receiver_id = c.Int(),
                        Value = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Transactions_id)
                .ForeignKey("dbo.Account", t => t.Receiver_id)
                .ForeignKey("dbo.Account", t => t.Sender_id)
                .Index(t => t.Sender_id)
                .Index(t => t.Receiver_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transaction", "Sender_id", "dbo.Account");
            DropForeignKey("dbo.Transaction", "Receiver_id", "dbo.Account");
            DropIndex("dbo.Transaction", new[] { "Receiver_id" });
            DropIndex("dbo.Transaction", new[] { "Sender_id" });
            DropTable("dbo.Transaction");
        }
    }
}
