// <auto-generated />
namespace Bank.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddingAccounts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddingAccounts));
        
        string IMigrationMetadata.Id
        {
            get { return "201611111333254_AddingAccounts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
