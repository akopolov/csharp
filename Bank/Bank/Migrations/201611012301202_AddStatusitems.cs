namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusitems : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Status(Status_Name)VALUES('Unverified')");
            Sql("INSERT INTO Status(Status_Name)VALUES('Active')");
            Sql("INSERT INTO Status(Status_Name)VALUES('Blocked')");
        }
        
        public override void Down()
        {
        }
    }
}
