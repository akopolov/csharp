namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingStrLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Action", "Action_type", c => c.String(nullable: false, maxLength: 2000));
            AlterColumn("dbo.Action", "Info", c => c.String(nullable: false, maxLength: 2000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Action", "Info", c => c.String(nullable: false));
            AlterColumn("dbo.Action", "Action_type", c => c.String(nullable: false));
        }
    }
}
