﻿using System.Web.Mvc;
using Bank.ViewModels;
using System.Web.Security;
using BL.Domain;
using BL.Service;

namespace Bank.Controllers
{
    public class UserController : Controller
    {
        private UserService _userService = new UserService();

        [Authorize]
        public ActionResult Index()
        {
            return View(_userService.GetLoggedInUser());
        }

        // GET: User/Login
        public ActionResult Login()
        {
            return View(new UserLoginModel());
        }

        //TODO: users that have status "Removed" or "blocked" should not have a ability to login.
        //TODO: Normal "Users" and "Admins" should have 2 difrent pages.
        [HttpPost]
        public ActionResult LoginUser(UserLoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Login", loginModel);
            }

            // gets login response string. NULL if success
            string response = _userService.LoginUserResponse(loginModel.UserLogin, loginModel.UserPassword);
            if (response != null)
            {
                loginModel.ResponseMessage = response;
                return View("Login", loginModel);
            }
            FormsAuthentication.SetAuthCookie(loginModel.UserLogin, false);
            if (_userService.isAdmin(loginModel.UserLogin))
            {
                return RedirectToAction("Index","Admin");
            }
            else
            {
                return RedirectToAction("Index","User");
            }
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: User/Register
        public ActionResult NewUser()
        {
            UserViewModel registerViewModel = new UserViewModel();
            registerViewModel.FillVM(new User(), null);

            return View("Userform", registerViewModel);
        }

        //POST: User/Register
        //Saves New USERS or Update ODL
        [HttpPost]
        public ActionResult Save(UserViewModel viewmodel)
        {
            if (!ModelState.IsValid)
            {
                return View("Userform", viewmodel);
            }

            // gets response string. NULL if success
            string response = _userService.RegisterUserResponse(viewmodel.User);
            if (response != null)
            {
                viewmodel.ErrorMessage = response;
                return View("Userform", viewmodel);
            }

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult AuthTest()
        {
            return View();
        }
    }
}