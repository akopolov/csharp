﻿using BL.Service;
using Bank.ViewModels;
using System.Web.Mvc;

namespace Bank.Controllers
{
    public class AccountController : Controller
    {
        private AccountService _accountService = new AccountService();
        private UserService _userService = new UserService();
        private LoggerService _loggerService = new LoggerService();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult CreateAccount()
        {
            return View(new UserCreateAccountModel { });
        }
        [Authorize]
        [HttpPost]
        public ActionResult SaveAccount(UserCreateAccountModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("CreateAccount");
            }
            viewModel.SaveAccount();

            return RedirectToAction("ViewAccounts");
        }

        [Authorize]
        public ActionResult ViewAccounts()
        {
            return View(_accountService.ViewAccounts());
        }

        public ActionResult Remove(int id)
        {
            if (!_accountService.Remove(id))
            {
                return HttpNotFound();
            }
            return RedirectToAction("ViewAccounts");
        }

        [Authorize]
        public ActionResult Payment()
        {
            PaymentViewModel viewModel = new PaymentViewModel();
            viewModel.FillVM();
            return View(viewModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult MakePayment(PaymentViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.FillVM();
                return View("Payment", viewModel);
            }
            viewModel.MakeTransaction();

            return RedirectToAction("PaymentInfo", viewModel);
        }

        [Authorize]
        public ActionResult PaymentInfo(PaymentViewModel viewModel)
        {
            viewModel.FillVM();
            return View(viewModel);
        }
        [Authorize]
        public ActionResult TransactionHistory()
        {
            TransactionHistoryViewModel viewModel = new TransactionHistoryViewModel();
            viewModel.FillVM();
            return View(viewModel);
        }
        [Authorize]
        public ActionResult TransactionHistoryResult(TransactionHistoryViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.FillVM();
                return View("TransactionHistory", viewModel);
            }
            TransactionHistoryResultViewModel resultViewModel = new TransactionHistoryResultViewModel();
            resultViewModel.FillVM(viewModel.AccNr);
            return View(resultViewModel);
        }
        public ActionResult UserInfo()
        {
            DetailsModel model = new DetailsModel();
            model.FillVM(_userService.GetLoggedInUser().User_id);
            return View("UserDetails", model);
        }
    }
}