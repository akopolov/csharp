﻿using System.Web.Mvc;
using Bank.ViewModels;
using BL.Domain;
using BL.Service;

namespace Bank.Controllers
{
    public class AdminController : Controller
    {
        private AdminService _adminService = new AdminService();
        private AccountService _accountService = new AccountService();
        private UserService _userService = new UserService();

        public ActionResult Index()
        {
            return View(_userService.GetLoggedInUser());
        }
        public ActionResult Users(int? page)
        {
            UserListViewModel adminmodel = new UserListViewModel();
            adminmodel.FillVM(page);
            return View("Users", adminmodel);
        }

        public ActionResult RemoveUser(int id)
        {
            if (_adminService.RemoveUser(id))
            {
                return HttpNotFound();
            }
            else
            {
                return RedirectToAction("Users");
            }
        }
        public ActionResult Edit(int id)
        {
            EditViewModel editmodel = new EditViewModel();
            editmodel.FillVM(id);
            return View("Edit", editmodel);
        }
        public ActionResult Save(EditViewModel editmodel)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", editmodel);
            }
            else
            {
                _adminService.Edit(editmodel.UserInfo, editmodel.UserTypeConnection);
                return RedirectToAction("Users");
            }
        }
        public ActionResult Details(int id)
        {
            User user = _adminService.GetUser(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            else
            { 
                DetailsModel viewModel = new DetailsModel();
                viewModel.FillVM(id);
                return View("Details", viewModel);
            }
        }
        public ActionResult Transactions(int? page)
        {
            TransactionListViewModel model = new TransactionListViewModel();
            model.FillVM(page);
            return View("Transactions", model);
        }
        public ActionResult AccountHistory(int id)
        {
            TransactionHistoryResultViewModel resultViewModel = new TransactionHistoryResultViewModel();
            resultViewModel.FillVM(_accountService.GetAccountByAccId(id).Account_number);
            return View("AccountHistory", resultViewModel);
        }
        public ActionResult RemoveAccount(int id)
        {
            string path = Request.UrlReferrer.AbsolutePath;
            if (!_accountService.Remove(id))
            {
                return HttpNotFound();
            }
            else
            {
                return Redirect(path);
            }
        }
        public ActionResult ActivateAccount(int id)
        {
            string path = Request.UrlReferrer.AbsolutePath;
            if (!_accountService.Activate(id))
            {
                return HttpNotFound();
            }
            else
            {
                return Redirect(path);
            }
        }
        public ActionResult CancelTransaction(int id)
        {
            string path = Request.UrlReferrer.AbsolutePath;
            if (!_adminService.CancelTransaction(id))
            {
                return HttpNotFound();
            }
            else
            {
                return Redirect(path);
            }
        }
        public ActionResult Logger(int? page)
        {
            LoggerViewModel model = new LoggerViewModel();
            model.FillVM(page);
            return View("Logger", model);
        }
    }
}