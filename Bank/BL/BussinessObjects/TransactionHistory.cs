﻿using System;

namespace BL.BussinessObjects
{
    public class TransactionHistory
    {
        public DateTime Transaction_date { get; set; }
        public double Value { get; set; }
        public string Comment { get; set; }
        public string TransactionAccount { get; set; }
        public bool IsSender { get; set; }
        public double CurrentValue { get; set; }

    }
}
