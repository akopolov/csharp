﻿using BL.Domain;
using BL.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
namespace BL.Service
{
    public class UserService
    {
        private LoggerService _loggerService = new LoggerService();

        public User GetLoggedInUser()
        {
            User user;
            string userName = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

            using (BankContext db = new BankContext())
            {
                user = db.Users.Where(x => x.User_login == userName).FirstOrDefault();
            }
            return user;
        }
        // Used for logging in. null if success, otherwise repsonse string
        public string LoginUserResponse(string userLogin, string userPassword)
        {
            User user = GetLoginUser(userLogin, userPassword);
            string response = null;

            if (user == null)
            {
                response = "Invalid Username/Password";
                _loggerService.Log(Resources.USER_LOGIN_FAIL, "Username \"" + userLogin + "\" failed to log in");
                return response;
            }

            if (!Verify(user))
            {
                response = VerificationFailMessage(user);
                _loggerService.Log(Resources.USER_LOGIN_FAIL, "Username \"" + userLogin + "\" failed to log in");
                return response;
            }

            _loggerService.Log(Resources.USER_LOGIN, "User \"" + user.User_login + "\" logged in", user.User_id);
            return response;
        }

        // Tries to find user with username and password
        private User GetLoginUser(string userLogin, string userPassword)
        {
            User user;
            using (BankContext db = new BankContext())
            {
                user = db.Users.Where(x => x.User_login == userLogin && x.User_password == userPassword).FirstOrDefault();
            }

            return user;
        }

        // Tries to register user, if succeeds returns null, otherwise reponse message
        public string RegisterUserResponse(User u)
        {
            string response = null;

            if (!isUsernameAvailable(u.User_login))
            {
                response = "Login name is taken";
                return response;
            }

            addUser(u);
            addType(u);

            return response;
        }
        public List<Status> GetStatus()
        {
            List<Status> statuses;
            using (BankContext db = new BankContext())
            {
                statuses = db.Statuses.ToList();
            }

            return statuses;
        }
        private void addUser(User user)
        {
            using (BankContext db = new BankContext())
            {
                user.Status_id = Status.Unverified;
                user.Online = false;
                db.Users.Add(user);
                db.SaveChanges();

                _loggerService.Log(Resources.USER_CREATED, "User \"" + user.User_login + "\" created", user.User_id);
            }
        }
        public User GetUser(int id)
        {
            User user;
            using (BankContext db = new BankContext())
            {
                user = db.Users.SingleOrDefault(u => u.User_id == id);
            }
            return user;
        }
        private void addType(User user)
        {
            User_type usertype = new User_type()
            {
                User_ID = user.User_id,
                From_date = DateTime.Now,
                To_date = DateTime.Now.AddDays(3),
                Type_ID = Type_u.User_type
            };
            usertype.To_date.AddDays(3);
            using (BankContext db = new BankContext())
            {
                db.User_type_connection.Add(usertype);
                db.SaveChanges();
            }
        }
        public List<Type_u> GetAllTypes()
        {
            List<Type_u> types;
            using (BankContext db = new BankContext())
            {
                types = db.Type_of_users.ToList();
            }
            return types;
        }

        public bool Verify(User u)
        {
            if (u.Status_id == Status.Active || u.Status_id == Status.Unverified)
            {
                return true;
            }
            return false;
        }

        // Checks if username is available for registration
        public bool isUsernameAvailable(string login)
        {
            User u;
            using (BankContext db = new BankContext())
            {
                u = db.Users.SingleOrDefault(c => c.User_login == login);
            }
            if (u == null)
            {
                return true;
            }
            return false;
        }
        public string VerificationFailMessage(User u)
        {
            if (u.Status_id == Status.Blocked)
            {
                return "Your account has been temporarily suspended";
            }
            else
            {
                return "Your account has been disabled";
            }
        }
        public bool isAdmin(string login)
        {
            User u;
            User_type type;
            using (BankContext db = new BankContext())
            {
                u = db.Users.SingleOrDefault(c => c.User_login == login);
                type = db.User_type_connection.SingleOrDefault(c=>c.User_ID==u.User_id);
            }
            if (type.Type_ID==Type_u.Admin_type)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }
}