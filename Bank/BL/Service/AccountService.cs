﻿using BL.Domain;
using BL.Properties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BL.Service
{
    public class AccountService
    {
        private UserService _userService = new UserService();
        private LoggerService _loggerService = new LoggerService();

        public void SaveAccount(double balance)
        {
            Account account = new Account();
            User u = _userService.GetLoggedInUser();
            account.Account_number = GenerateAccNr(u);
            account.User_id = u.User_id;
            account.Account_balance = balance;
            account.IsActive = true;

            using (BankContext db = new BankContext())
            {
                db.Accounts.Add(account);
                db.SaveChanges();
            }

            _loggerService.Log(Resources.ACCOUNT_CREATE
                    , "Starting balance: " + account.Account_balance, _userService.GetLoggedInUser().User_id);
        }

        public List<Account> ViewAccounts()
        {
            User user = _userService.GetLoggedInUser();
            List<Account> accounts;

            using (BankContext db = new BankContext())
            {
                accounts = db.Accounts.Where(a => a.User_id == user.User_id && a.IsActive).ToList();
            }

            _loggerService.Log(Resources.ACCOUNT_VIEW,
                "User viewed accounts", _userService.GetLoggedInUser().User_id);

            return accounts;
        }


        public bool Remove(int id)
        {
            using (BankContext db = new BankContext())
            {
                Account account = db.Accounts.Single(a => a.Account_id == id);
                if (account == null)
                {
                    return false;
                }
                account.IsActive = false;
                db.SaveChanges();

                _loggerService.Log(Resources.ACCOUNT_REMOVE,
                "Deleted account nr: " + account.Account_number, _userService.GetLoggedInUser().User_id, account.Account_id);
            }

            return true;
        }

        public bool Activate(int id)
        {
            using (BankContext db = new BankContext())
            {
                Account account = db.Accounts.Single(a => a.Account_id == id);
                if (account == null)
                {
                    return false;
                }
                account.IsActive = true;
                db.SaveChanges();
                _loggerService.Log(Resources.ACCOUNT_REACTIVATED, 
                "Reactivated account nr: " + account.Account_number, _userService.GetLoggedInUser().User_id, account.Account_id);
            }
            return true;
        }


        public List<string> GetUserAccLists()
        {
            User user = _userService.GetLoggedInUser();
            List<string> userAccs = null;

            using (BankContext db = new BankContext())
            {
                userAccs = db.Accounts
                    .Where(a => a.User_id == user.User_id && a.IsActive)
                    .Select(a => a.Account_number)
                    .ToList();
            }

            return userAccs;
        }


        public List<string> GetAllAccLists()
        {
            List<string> allAccs = null;

            using (BankContext db = new BankContext())
            {
                allAccs = db.Accounts
                .Where(a => a.IsActive)
                .Select(a => a.Account_number)
                .ToList();
            }

            return allAccs;
        }


        // Gets User to whom the Account belongs
        public User GetUserByAccount(Account acc)
        {
            User user = null;
            using (BankContext db = new BankContext())
            {
                user = db.Users
                    .Where(u => u.User_id == acc.User_id)
                    .SingleOrDefault();
            }

            return user;
        }


        // get Account by account number
        public Account GetAccountByAccNr(string accNr)
        {
            Account acc = null;

            using (BankContext db = new BankContext())
            {
                acc = db.Accounts
                .Where(a => a.Account_number == accNr)
                .FirstOrDefault();
            }

            return acc;
        }

        // get Account by account id
        public Account GetAccountByAccId(int? id)
        {
            Account acc = null;

            using (BankContext db = new BankContext())
            {
                acc = db.Accounts
                .Where(a => a.Account_id == id)
                .FirstOrDefault();
            }

            return acc;
        }


        // Gets User to whom the Account number belongs
        public User GetUserByAccountNr(string accNr)
        {
            Account acc = GetAccountByAccNr(accNr);
            return GetUserByAccount(acc);
        }


        // Generate "random" bankaccount number
        private string GenerateAccNr(User u)
        {
            string accNr = "";
            accNr += u.User_firstname.ToString().ToUpper();
            accNr += u.User_lastname.First().ToString().ToUpper();
            accNr += DateTime.Now.ToString("MMddhhmmss");
            return accNr;
        }


        // rounds incoming balance string to two decimal places
        private string RoundTwoDecimalsFromString(string balance)
        {
            double b;
            if (double.TryParse(balance, out b)) { }
            b = Math.Round(b, 2, MidpointRounding.AwayFromZero);
            return b.ToString();
        }
    }
}
