﻿using BL.BussinessObjects;
using BL.Domain;
using BL.Properties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BL.Service
{
    public class TransactionService
    {
        private AccountService _accountService = new AccountService();
        private LoggerService _loggerService = new LoggerService();
        private UserService _userService = new UserService();


        // perform transaction between accounts
        public void MakeTransaction(string senderAccNr, string receiverAccNr, double amount, string comment)
        {
            Account senderAccount = _accountService.GetAccountByAccNr(senderAccNr);
            Account receiverAccount = _accountService.GetAccountByAccNr(receiverAccNr);

            // update balance in db
            using (BankContext db = new BankContext())
            {
                Account sAcc = db.Accounts.Where(a => a.Account_id == senderAccount.Account_id).FirstOrDefault();
                Account rAcc = db.Accounts.Where(a => a.Account_id == receiverAccount.Account_id).FirstOrDefault();
                sAcc.Account_balance -= amount;
                rAcc.Account_balance += amount;
                db.SaveChanges();
            }

            SaveTransactionHistory(senderAccount, receiverAccount, amount, comment);
        }

        // Returns accounts history of transactions
        public List<TransactionHistory> GetTransactionHistory(string accNr)
        {
            Account acc = _accountService.GetAccountByAccNr(accNr);
            List<Transaction> transactionList = GetAccountTransactions(acc);
            List<TransactionHistory> historyList = TransactionToHistory(transactionList, acc);

            _loggerService.Log(Resources.TRANSACTION_HISTORY, "History of account: " + acc.Account_number,
                    _userService.GetLoggedInUser().User_id, acc.Account_id);

            return historyList;
        }

        // Gets account starting balance
        public double GetTransactionStartValue(string accNr)
        {
            Account acc = _accountService.GetAccountByAccNr(accNr);
            List<Transaction> transactionList = GetAccountTransactions(acc);

            double accountBalance = 0;

            foreach (Transaction t in transactionList)
            {
                if (t.Sender_id.Equals(acc.Account_id))
                {
                    // add
                    accountBalance += t.Value;
                }
                else
                {
                    // subtract
                    accountBalance -= t.Value;
                }
            }

            // add current account balance
            accountBalance += acc.Account_balance;
            return accountBalance;
        }

        // Turns Transaction to TransactionHistory
        private List<TransactionHistory> TransactionToHistory(List<Transaction> transactionList, Account acc)
        {
            List<TransactionHistory> historyList = new List<TransactionHistory>();
            double CurrentValue = GetTransactionStartValue(acc.Account_number);

            foreach (Transaction t in transactionList)
            {
                TransactionHistory th = new TransactionHistory();
                th.Comment = t.Comment;
                th.Value = t.Value;
                th.Transaction_date = t.Transaction_date;
                if (t.Sender_id.Equals(acc.Account_id))
                {
                    th.IsSender = true;
                    th.TransactionAccount = _accountService.GetAccountByAccId(t.Receiver_id).Account_number; 
                    th.CurrentValue = CurrentValue - t.Value;
                    CurrentValue = th.CurrentValue;
                }
                else
                {
                    th.IsSender = false;
                    th.TransactionAccount = _accountService.GetAccountByAccId(t.Sender_id).Account_number;
                    th.CurrentValue = CurrentValue + t.Value;
                    CurrentValue = th.CurrentValue;
                }
                historyList.Add(th);
            }

            return historyList;
        }

        // Gets all transactions associated with given account
        private List<Transaction> GetAccountTransactions(Account acc)
        {
            List<Transaction> transactionList = new List<Transaction>();

            using (BankContext db = new BankContext())
            {
                transactionList = db.Transaction.Where(t => t.Receiver_id == acc.Account_id
                                     || t.Sender_id == acc.Account_id)
                                    .ToList();
            }

            return transactionList;
        }


        // saves transaction to database
        private void SaveTransactionHistory(Account senderAccount, Account receiverAccount, double amount, string comment)
        {
            Transaction t = new Transaction();
            t.Transaction_date = DateTime.Now;
            t.Sender_id = senderAccount.Account_id;
            t.Receiver_id = receiverAccount.Account_id;
            t.Cancel = false;
            t.Value = amount;
            t.Comment = comment;

            using (BankContext db = new BankContext())
            {
                db.Transaction.Add(t);
                db.SaveChanges();

                _loggerService.Log(Resources.TRANSACTION, "Amount: " + amount.ToString(),
                            _userService.GetLoggedInUser().User_id, senderAccount.Account_id, t.Transactions_id);
            }

        }

    }
}
