﻿using BL.Domain;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BL.Service
{
    public class LoggerService
    {

        // Logs actionType, Info
        public void Log(string actionType, string info)
        {
            Domain.Action a = new Domain.Action();
            a.Action_date = DateTime.Now;
            a.Action_type = actionType;
            a.Info = info;
            SaveLog(a);
        }

        // Logs actionType, info, userId
        public void Log(string actionType, string info, int userId)
        {
            Domain.Action a = new Domain.Action();
            a.Action_date = DateTime.Now;
            a.Action_type = actionType;
            a.Info = info;
            a.User_ID = userId;
            SaveLog(a);
        }

        // Logs actionType, info, userId, accountId
        public void Log(string actionType, string info, int userId, int accountId)
        {
            Domain.Action a = new Domain.Action();
            a.Action_date = DateTime.Now;
            a.Action_type = actionType;
            a.Info = info;
            a.User_ID = userId;
            a.Account_ID = accountId;
            SaveLog(a);
        }
        // Logs actionType, info, userId, accountId, transactionId
        public void Log(string actionType, string info, int userId, int? accountId, int transactionId)
        {
            Domain.Action a = new Domain.Action();
            a.Action_date = DateTime.Now;
            a.Action_type = actionType;
            a.Info = info;
            a.User_ID = userId;
            a.Account_ID = accountId;
            a.Transaction_ID = transactionId;
            SaveLog(a);
        }
        // Inserts Action to db
        private void SaveLog(Domain.Action a)
        {
            using (BankContext db = new BankContext())
            {
                db.Actions.Add(a);
                db.SaveChanges();
            }
        }

    }
}
