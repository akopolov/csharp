﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.Domain;
using System.Data.Entity;
using BL.Properties;

namespace BL.Service
{
    public class AdminService
    {
        private LoggerService _loggerService = new LoggerService();
        private UserService _userService = new UserService();
        private TransactionService _transactionService = new TransactionService();

        public IEnumerable<User> GetUsersList()
        {
            IEnumerable<User> users;
            using (BankContext db = new BankContext())
            {
                users = db.Users.Include(c => c.Status).ToList();
            }

            _loggerService.Log(Resources.ADMIN_USERS_VIEW, "Admin: " + _userService.GetLoggedInUser().User_login
                , _userService.GetLoggedInUser().User_id);
            return users;
        }
        public IEnumerable<Status> GetStatusList()
        {
            IEnumerable<Status> statuses;
            using (BankContext db = new BankContext())
            {
                statuses = db.Statuses.ToList();
            }
            return statuses;
        }
        public IEnumerable<User_type> GetUserTypeList()
        {
            IEnumerable<User_type> user_types;
            using (BankContext db = new BankContext())
            {
                user_types = db.User_type_connection.Include(c => c.Type_of_user).ToList();
            }
            return user_types;
        }
        public IEnumerable<Type_u> GetUserTypes()
        {
            IEnumerable<Type_u> types;
            using (BankContext db = new BankContext())
            {
                types = db.Type_of_users.ToList();
            }
            return types;
        }
        public bool RemoveUser(int id)
        {
            using (BankContext db = new BankContext())
            {
                User user_r = db.Users.Single(u => u.User_id == id);
                if (user_r == null)
                {
                    return true;
                }
                user_r.Status_id = Status.Removed;
                db.SaveChanges();

                _loggerService.Log(Resources.ADMIN_USER_REMOVE, "Admin: " + _userService.GetLoggedInUser().User_login
                    + " removed user: " + user_r.User_login
                    , _userService.GetLoggedInUser().User_id);

                return false;
            }
        }
        public IEnumerable<Account> GetUserAccounts(int id)
        {
            IEnumerable<Account> list;
            using (BankContext db = new BankContext())
            {
                list = db.Accounts.Where(c => c.User_id == id).ToList();
            }

            _loggerService.Log(Resources.ADMIN_USER_VIEW, "Admin viewed user: " + _userService.GetUser(id).User_login
                    , _userService.GetLoggedInUser().User_id);

            return list;
        }
        public User_type GetUserTypeOne(int id)
        {
            User_type user_type_d;
            using (BankContext db = new BankContext())
            {
                user_type_d = db.User_type_connection.Include(c => c.Type_of_user).SingleOrDefault(c => c.User_ID == id);
            }
            return user_type_d;
        }

        public IEnumerable<User_type> GetUserType(int id)
        {
            IEnumerable<User_type> user_type_d;
            using (BankContext db = new BankContext())
            {
                user_type_d = db.User_type_connection.Include(c => c.Type_of_user).Where(c => c.User_ID == id).ToList();
            }
            return user_type_d;
        }

        //!!!!Dose not check if the "LOGIN" is taken 
        public void Edit(User u, User_type u_type)
        {
            using (BankContext db = new BankContext())
            {
                User UserInDb = db.Users.SingleOrDefault(c => c.User_id == u.User_id);
                UserInDb.User_firstname = u.User_firstname;
                UserInDb.User_lastname = u.User_lastname;
                UserInDb.User_password = u.User_password;
                UserInDb.User_login = u.User_login;
                UserInDb.Status_id = u.Status.Status_id;

                //!!!!! 1 and same user can have more that 1 "User type"!!!
                User_type TypeInDb = db.User_type_connection.SingleOrDefault(d => d.User_ID == u.User_id);
                TypeInDb.From_date = DateTime.Now;
                TypeInDb.To_date = DateTime.Now.AddDays(3);
                TypeInDb.Type_ID = u_type.Type_of_user.Type_ID;
                db.SaveChanges();
            }

            _loggerService.Log(Resources.ADMIN_USER_EDIT, "Admin edited user: " + u.User_login
                    , _userService.GetLoggedInUser().User_id);
        }
        public User GetUser(int id)
        {
            User user_d;
            using (BankContext db = new BankContext())
            {
                user_d = db.Users.Include(c => c.Status).SingleOrDefault(c => c.User_id == id);
            }
            return user_d;
        }
        public IEnumerable<Transaction> GetTransactionsList()
        {
            IEnumerable<Transaction> trans_list;
            using (BankContext db = new BankContext())
            {
                trans_list = db.Transaction.Include(c => c.Receiver).Include(d => d.Sender).OrderBy(f => f.Transaction_date).ToList();
            }

            _loggerService.Log(Resources.ADMIN_VIEWED_ALL_TRANSACTIONS, "Admin: " + _userService.GetLoggedInUser().User_login
                        , _userService.GetLoggedInUser().User_id);

            return trans_list;
        }
        public IEnumerable<Domain.Action> GetLoggerList()
        {
            IEnumerable<Domain.Action> action_list;
            using (BankContext db = new BankContext())
            {
                action_list = db.Actions.Include(c=>c.User_action)
                                    .Include(c=>c.User_action)
                                    .Include(c=>c.Account_action)
                                    .Include(c=>c.Transactions_action.Sender)
                                    .OrderBy(c=>c.Action_date).ToList();
            }
            _loggerService.Log(Resources.ADMIN_LOGGER_VIEW, "Admin: " + _userService.GetLoggedInUser().User_login
                        , _userService.GetLoggedInUser().User_id);

            return action_list;
        }
        public bool CancelTransaction(int id)
        {
            Transaction old_trans;
            using (BankContext db = new BankContext())
            {
                old_trans = db.Transaction.Include(c=>c.Receiver)
                                          .Include(c=>c.Sender)
                                          .SingleOrDefault(c=>c.Transactions_id==id);
                if (old_trans == null)
                {
                    return false;
                }
                old_trans.Cancel = true;
                old_trans.Comment = "Transaction was canceld by admin: "+_userService.GetLoggedInUser().User_login;
                db.SaveChanges();
                _loggerService.Log(Resources.TRANSACTION_CANCEL, "Admin:"+_userService.GetLoggedInUser().User_login,
                _userService.GetLoggedInUser().User_id,null,old_trans.Transactions_id);
                _transactionService.MakeTransaction(old_trans.Receiver.Account_number,old_trans.Sender.Account_number,old_trans.Value,
                "Refund");
            }
            return true;
        }
    }
}
