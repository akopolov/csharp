﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Domain
{
    public class Type_u
    {
        [Key]
        public int Type_ID { get; set; }
        [StringLength(20)]
        public string Type_name { get; set; }

        public static readonly int Admin_type = 1;
        public static readonly int User_type = 2;
    }
}
