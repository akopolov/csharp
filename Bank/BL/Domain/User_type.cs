﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Domain
{
    //connects 2 tables "User" and "Type_u"
    public class User_type
    {
        [Key]
        public int User_type_ID { get; set; }
        public int User_ID { get; set; }
        public int Type_ID{ get; set; }

        public DateTime From_date { get; set; }
        public DateTime To_date { get; set; }

        [ForeignKey("User_ID")]
        public User User_id_number { get; set; }

        [Display(Name = "User Type")]
        [ForeignKey("Type_ID")]
        public Type_u Type_of_user { get; set; }
    }
}
