﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BL.Domain
{
    public class Status
    {
        [Key]
        public int Status_id { get; set; }

        [StringLength(20)]
        public string Status_name { get; set; }

        //static DATA in STATUSES TALE
        public static readonly int Unverified = 1;
        public static readonly int Active = 2;
        public static readonly int Blocked = 3;
        public static readonly int Removed = 4;
    }
}