﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BL.Domain
{
    public class Transaction
    {
        [Key]
        public int Transactions_id { get; set; }
        public DateTime Transaction_date { get; set; }
        public int? Sender_id { get; set; }
        public int? Receiver_id { get; set; }
        [RegularExpression(@"\d+(\,\d{1,2})?", ErrorMessage = "Invalid price")]
        public double Value { get; set; }
        public string Comment { get; set; }
        public bool Cancel { get; set; }
        [ForeignKey("Sender_id")]
        public Account Sender { get; set; }
        [ForeignKey("Receiver_id")]
        public Account Receiver { get; set; }
    }
}