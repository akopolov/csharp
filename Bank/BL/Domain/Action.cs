﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BL.Domain
{
    public class Action
    {
        [Key]
        public int Action_ID { get; set; }
        public DateTime Action_date { get; set; }
        [Required(ErrorMessage = "Action_type is required.")]
        [StringLength(2000)]
        public string Action_type { get; set; }
        [Required(ErrorMessage = "Info is required.")]
        [StringLength(2000)]
        public string Info { get; set; }
        public int? User_ID { get; set; }
        public int? Account_ID { get; set; }
        public int? Transaction_ID { get; set; }
        [ForeignKey("User_ID")]
        public User User_action { get; set; }
        [ForeignKey("Account_ID")]
        public Account Account_action { get; set; }
        [ForeignKey("Transaction_ID")]
        public Transaction Transactions_action { get; set; }
    }
}
