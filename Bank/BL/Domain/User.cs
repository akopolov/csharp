﻿using System.ComponentModel.DataAnnotations;


namespace BL.Domain
{
    public class User
    {
        [Key]
        public int User_id { get; set; }
        
        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Login")]
        [StringLength(20)]
        public string User_login { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password")]
        [StringLength(20, MinimumLength = 9, ErrorMessage = "Password must be 9-20 characters long")]
        [DataType(DataType.Password)]
        public string User_password { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First name")]
        [StringLength(20)]
        public string User_firstname { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last name")]
        [StringLength(20)]
        public string User_lastname { get; set; }

        public bool Online { get; set; }

        [Display(Name ="Status Type")]
        public int Status_id { get; set; }

       public Status Status { get; set; }
    }
}