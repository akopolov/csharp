﻿
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BL.Domain
{
    public class BankContext : DbContext
    {

        public BankContext() : base("BankDB")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<Type_u> Type_of_users { get; set; }
        public DbSet<User_type> User_type_connection { get; set; }
        public DbSet<Action> Actions { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}