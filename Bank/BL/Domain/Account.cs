﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BL.Domain
{
    public class Account
    {
        [Key]
        public int Account_id { get; set; }
        public int User_id { get; set; }
        public string Account_number { get; set; }
        [RegularExpression(@"\d+(\,\d{1,2})?", ErrorMessage = "Invalid price")]
        public double Account_balance { get; set; }
        public User User { get; set; }
        public bool IsActive { get; set; }
    }
}